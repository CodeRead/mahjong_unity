﻿
public class MjActionData
{
    public int m_RoundId; //牌局id
    public int m_Action; //行为
    public int m_Index; //行为人, 比如: A出牌, 发牌给A, 行为人都是A
    public int[] m_CardIds; //吃碰杠时(自己的那2或3牌)
    public int m_CardId; //发牌, 出牌, 吃碰杠时(别人的牌)
    public int m_LastChuIndex;
    public int[] m_NextAction; //针对此次行为我可以进行的操作, 比如: B出1万, 我可以碰; 自己摸牌(2万), 我可以杠或者自摸;
}
