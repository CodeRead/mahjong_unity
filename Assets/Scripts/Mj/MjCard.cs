﻿
using System.Collections.Generic;
using UnityEngine;

//万筒条分别为36张, 东西南北16张, 中发白12张, 总共136张
//每人13张手牌, 52张, 还剩84张牌; 不碰, 不吃, 最多出牌21张
//上家连续碰碰4次, 最多25张牌 
public class MjCard
{
    public readonly static MjCard Empty = new MjCard(0, 0, 0); //空牌, 我看不到别人的手牌, 所以别人的手牌会用空牌占位

    public readonly static MjCard Tong_1_1 = new MjCard(1, 0, 1);
    public readonly static MjCard Tong_1_2 = new MjCard(2, 0, 1);
    public readonly static MjCard Tong_1_3 = new MjCard(3, 0, 1);
    public readonly static MjCard Tong_1_4 = new MjCard(4, 0, 1);

    public readonly static MjCard Tong_2_1 = new MjCard(5, 0, 2);
    public readonly static MjCard Tong_2_2 = new MjCard(6, 0, 2);
    public readonly static MjCard Tong_2_3 = new MjCard(7, 0, 2);
    public readonly static MjCard Tong_2_4 = new MjCard(8, 0, 2);

    public readonly static MjCard Tong_3_1 = new MjCard(9, 0, 3);
    public readonly static MjCard Tong_3_2 = new MjCard(10, 0, 3);
    public readonly static MjCard Tong_3_3 = new MjCard(11, 0, 3);
    public readonly static MjCard Tong_3_4 = new MjCard(12, 0, 3);

    public readonly static MjCard Tong_4_1 = new MjCard(13, 0, 4);
    public readonly static MjCard Tong_4_2 = new MjCard(14, 0, 4);
    public readonly static MjCard Tong_4_3 = new MjCard(15, 0, 4);
    public readonly static MjCard Tong_4_4 = new MjCard(16, 0, 4);

    public readonly static MjCard Tong_5_1 = new MjCard(17, 0, 5);
    public readonly static MjCard Tong_5_2 = new MjCard(18, 0, 5);
    public readonly static MjCard Tong_5_3 = new MjCard(19, 0, 5);
    public readonly static MjCard Tong_5_4 = new MjCard(20, 0, 5);

    public readonly static MjCard Tong_6_1 = new MjCard(21, 0, 6);
    public readonly static MjCard Tong_6_2 = new MjCard(22, 0, 6);
    public readonly static MjCard Tong_6_3 = new MjCard(23, 0, 6);
    public readonly static MjCard Tong_6_4 = new MjCard(24, 0, 6);

    public readonly static MjCard Tong_7_1 = new MjCard(25, 0, 7);
    public readonly static MjCard Tong_7_2 = new MjCard(26, 0, 7);
    public readonly static MjCard Tong_7_3 = new MjCard(27, 0, 7);
    public readonly static MjCard Tong_7_4 = new MjCard(28, 0, 7);

    public readonly static MjCard Tong_8_1 = new MjCard(29, 0, 8);
    public readonly static MjCard Tong_8_2 = new MjCard(30, 0, 8);
    public readonly static MjCard Tong_8_3 = new MjCard(31, 0, 8);
    public readonly static MjCard Tong_8_4 = new MjCard(32, 0, 8);

    public readonly static MjCard Tong_9_1 = new MjCard(33, 0, 9);
    public readonly static MjCard Tong_9_2 = new MjCard(34, 0, 9);
    public readonly static MjCard Tong_9_3 = new MjCard(35, 0, 9);
    public readonly static MjCard Tong_9_4 = new MjCard(36, 0, 9);


    public readonly static MjCard Tiao_1_1 = new MjCard(37, 1, 1);
    public readonly static MjCard Tiao_1_2 = new MjCard(38, 1, 1);
    public readonly static MjCard Tiao_1_3 = new MjCard(39, 1, 1);
    public readonly static MjCard Tiao_1_4 = new MjCard(40, 1, 1);

    public readonly static MjCard Tiao_2_1 = new MjCard(41, 1, 2);
    public readonly static MjCard Tiao_2_2 = new MjCard(42, 1, 2);
    public readonly static MjCard Tiao_2_3 = new MjCard(43, 1, 2);
    public readonly static MjCard Tiao_2_4 = new MjCard(44, 1, 2);

    public readonly static MjCard Tiao_3_1 = new MjCard(45, 1, 3);
    public readonly static MjCard Tiao_3_2 = new MjCard(46, 1, 3);
    public readonly static MjCard Tiao_3_3 = new MjCard(47, 1, 3);
    public readonly static MjCard Tiao_3_4 = new MjCard(48, 1, 3);

    public readonly static MjCard Tiao_4_1 = new MjCard(49, 1, 4);
    public readonly static MjCard Tiao_4_2 = new MjCard(50, 1, 4);
    public readonly static MjCard Tiao_4_3 = new MjCard(51, 1, 4);
    public readonly static MjCard Tiao_4_4 = new MjCard(52, 1, 4);

    public readonly static MjCard Tiao_5_1 = new MjCard(53, 1, 5);
    public readonly static MjCard Tiao_5_2 = new MjCard(54, 1, 5);
    public readonly static MjCard Tiao_5_3 = new MjCard(55, 1, 5);
    public readonly static MjCard Tiao_5_4 = new MjCard(56, 1, 5);

    public readonly static MjCard Tiao_6_1 = new MjCard(57, 1, 6);
    public readonly static MjCard Tiao_6_2 = new MjCard(58, 1, 6);
    public readonly static MjCard Tiao_6_3 = new MjCard(59, 1, 6);
    public readonly static MjCard Tiao_6_4 = new MjCard(60, 1, 6);

    public readonly static MjCard Tiao_7_1 = new MjCard(61, 1, 7);
    public readonly static MjCard Tiao_7_2 = new MjCard(62, 1, 7);
    public readonly static MjCard Tiao_7_3 = new MjCard(63, 1, 7);
    public readonly static MjCard Tiao_7_4 = new MjCard(64, 1, 7);

    public readonly static MjCard Tiao_8_1 = new MjCard(65, 1, 8);
    public readonly static MjCard Tiao_8_2 = new MjCard(66, 1, 8);
    public readonly static MjCard Tiao_8_3 = new MjCard(67, 1, 8);
    public readonly static MjCard Tiao_8_4 = new MjCard(68, 1, 8);

    public readonly static MjCard Tiao_9_1 = new MjCard(69, 1, 9);
    public readonly static MjCard Tiao_9_2 = new MjCard(70, 1, 9);
    public readonly static MjCard Tiao_9_3 = new MjCard(71, 1, 9);
    public readonly static MjCard Tiao_9_4 = new MjCard(72, 1, 9);


    public readonly static MjCard Wan_1_1 = new MjCard(73, 2, 1);
    public readonly static MjCard Wan_1_2 = new MjCard(74, 2, 1);
    public readonly static MjCard Wan_1_3 = new MjCard(75, 2, 1);
    public readonly static MjCard Wan_1_4 = new MjCard(76, 2, 1);

    public readonly static MjCard Wan_2_1 = new MjCard(77, 2, 2);
    public readonly static MjCard Wan_2_2 = new MjCard(78, 2, 2);
    public readonly static MjCard Wan_2_3 = new MjCard(79, 2, 2);
    public readonly static MjCard Wan_2_4 = new MjCard(80, 2, 2);

    public readonly static MjCard Wan_3_1 = new MjCard(81, 2, 3);
    public readonly static MjCard Wan_3_2 = new MjCard(82, 2, 3);
    public readonly static MjCard Wan_3_3 = new MjCard(83, 2, 3);
    public readonly static MjCard Wan_3_4 = new MjCard(84, 2, 3);

    public readonly static MjCard Wan_4_1 = new MjCard(85, 2, 4);
    public readonly static MjCard Wan_4_2 = new MjCard(86, 2, 4);
    public readonly static MjCard Wan_4_3 = new MjCard(87, 2, 4);
    public readonly static MjCard Wan_4_4 = new MjCard(88, 2, 4);

    public readonly static MjCard Wan_5_1 = new MjCard(89, 2, 5);
    public readonly static MjCard Wan_5_2 = new MjCard(90, 2, 5);
    public readonly static MjCard Wan_5_3 = new MjCard(91, 2, 5);
    public readonly static MjCard Wan_5_4 = new MjCard(92, 2, 5);

    public readonly static MjCard Wan_6_1 = new MjCard(93, 2, 6);
    public readonly static MjCard Wan_6_2 = new MjCard(94, 2, 6);
    public readonly static MjCard Wan_6_3 = new MjCard(95, 2, 6);
    public readonly static MjCard Wan_6_4 = new MjCard(96, 2, 6);

    public readonly static MjCard Wan_7_1 = new MjCard(97, 2, 7);
    public readonly static MjCard Wan_7_2 = new MjCard(98, 2, 7);
    public readonly static MjCard Wan_7_3 = new MjCard(99, 2, 7);
    public readonly static MjCard Wan_7_4 = new MjCard(100, 2, 7);

    public readonly static MjCard Wan_8_1 = new MjCard(101, 2, 8);
    public readonly static MjCard Wan_8_2 = new MjCard(102, 2, 8);
    public readonly static MjCard Wan_8_3 = new MjCard(103, 2, 8);
    public readonly static MjCard Wan_8_4 = new MjCard(104, 2, 8);

    public readonly static MjCard Wan_9_1 = new MjCard(105, 2, 9);
    public readonly static MjCard Wan_9_2 = new MjCard(106, 2, 9);
    public readonly static MjCard Wan_9_3 = new MjCard(107, 2, 9);
    public readonly static MjCard Wan_9_4 = new MjCard(108, 2, 9);


    public readonly static MjCard Dong_1 = new MjCard(109, 3, 0);
    public readonly static MjCard Dong_2 = new MjCard(110, 3, 0);
    public readonly static MjCard Dong_3 = new MjCard(111, 3, 0);
    public readonly static MjCard Dong_4 = new MjCard(112, 3, 0);

    public readonly static MjCard Nan_1 = new MjCard(113, 3, 3);
    public readonly static MjCard Nan_2 = new MjCard(114, 3, 3);
    public readonly static MjCard Nan_3 = new MjCard(115, 3, 3);
    public readonly static MjCard Nan_4 = new MjCard(116, 3, 3);

    public readonly static MjCard Xi_1 = new MjCard(117, 3, 6);
    public readonly static MjCard Xi_2 = new MjCard(118, 3, 6);
    public readonly static MjCard Xi_3 = new MjCard(119, 3, 6);
    public readonly static MjCard Xi_4 = new MjCard(120, 3, 6);

    public readonly static MjCard Bei_1 = new MjCard(121, 3, 9);
    public readonly static MjCard Bei_2 = new MjCard(122, 3, 9);
    public readonly static MjCard Bei_3 = new MjCard(123, 3, 9);
    public readonly static MjCard Bei_4 = new MjCard(124, 3, 9);


    public readonly static MjCard Zhong_1 = new MjCard(125, 4, 0);
    public readonly static MjCard Zhong_2 = new MjCard(126, 4, 0);
    public readonly static MjCard Zhong_3 = new MjCard(127, 4, 0);
    public readonly static MjCard Zhong_4 = new MjCard(128, 4, 0);

    public readonly static MjCard Fa_1 = new MjCard(129, 4, 3);
    public readonly static MjCard Fa_2 = new MjCard(130, 4, 3);
    public readonly static MjCard Fa_3 = new MjCard(131, 4, 3);
    public readonly static MjCard Fa_4 = new MjCard(132, 4, 3);

    public readonly static MjCard Bai_1 = new MjCard(133, 4, 6);
    public readonly static MjCard Bai_2 = new MjCard(134, 4, 6);
    public readonly static MjCard Bai_3 = new MjCard(135, 4, 6);
    public readonly static MjCard Bai_4 = new MjCard(136, 4, 6);

    private static string[] Card_Type_Name = new string[] { "筒", "条", "万" };


    private readonly static List<MjCard> s_Cards = new List<MjCard>();

    static MjCard()
    {
        s_Cards.Add(MjCard.Tong_1_1);
        s_Cards.Add(MjCard.Tong_1_2);
        s_Cards.Add(MjCard.Tong_1_3);
        s_Cards.Add(MjCard.Tong_1_4);

        s_Cards.Add(MjCard.Tong_2_1);
        s_Cards.Add(MjCard.Tong_2_2);
        s_Cards.Add(MjCard.Tong_2_3);
        s_Cards.Add(MjCard.Tong_2_4);

        s_Cards.Add(MjCard.Tong_3_1);
        s_Cards.Add(MjCard.Tong_3_2);
        s_Cards.Add(MjCard.Tong_3_3);
        s_Cards.Add(MjCard.Tong_3_4);

        s_Cards.Add(MjCard.Tong_4_1);
        s_Cards.Add(MjCard.Tong_4_2);
        s_Cards.Add(MjCard.Tong_4_3);
        s_Cards.Add(MjCard.Tong_4_4);

        s_Cards.Add(MjCard.Tong_5_1);
        s_Cards.Add(MjCard.Tong_5_2);
        s_Cards.Add(MjCard.Tong_5_3);
        s_Cards.Add(MjCard.Tong_5_4);

        s_Cards.Add(MjCard.Tong_6_1);
        s_Cards.Add(MjCard.Tong_6_2);
        s_Cards.Add(MjCard.Tong_6_3);
        s_Cards.Add(MjCard.Tong_6_4);

        s_Cards.Add(MjCard.Tong_7_1);
        s_Cards.Add(MjCard.Tong_7_2);
        s_Cards.Add(MjCard.Tong_7_3);
        s_Cards.Add(MjCard.Tong_7_4);

        s_Cards.Add(MjCard.Tong_8_1);
        s_Cards.Add(MjCard.Tong_8_2);
        s_Cards.Add(MjCard.Tong_8_3);
        s_Cards.Add(MjCard.Tong_8_4);

        s_Cards.Add(MjCard.Tong_9_1);
        s_Cards.Add(MjCard.Tong_9_2);
        s_Cards.Add(MjCard.Tong_9_3);
        s_Cards.Add(MjCard.Tong_9_4);

        s_Cards.Add(MjCard.Tiao_1_1);
        s_Cards.Add(MjCard.Tiao_1_2);
        s_Cards.Add(MjCard.Tiao_1_3);
        s_Cards.Add(MjCard.Tiao_1_4);

        s_Cards.Add(MjCard.Tiao_2_1);
        s_Cards.Add(MjCard.Tiao_2_2);
        s_Cards.Add(MjCard.Tiao_2_3);
        s_Cards.Add(MjCard.Tiao_2_4);

        s_Cards.Add(MjCard.Tiao_3_1);
        s_Cards.Add(MjCard.Tiao_3_2);
        s_Cards.Add(MjCard.Tiao_3_3);
        s_Cards.Add(MjCard.Tiao_3_4);

        s_Cards.Add(MjCard.Tiao_4_1);
        s_Cards.Add(MjCard.Tiao_4_2);
        s_Cards.Add(MjCard.Tiao_4_3);
        s_Cards.Add(MjCard.Tiao_4_4);

        s_Cards.Add(MjCard.Tiao_5_1);
        s_Cards.Add(MjCard.Tiao_5_2);
        s_Cards.Add(MjCard.Tiao_5_3);
        s_Cards.Add(MjCard.Tiao_5_4);

        s_Cards.Add(MjCard.Tiao_6_1);
        s_Cards.Add(MjCard.Tiao_6_2);
        s_Cards.Add(MjCard.Tiao_6_3);
        s_Cards.Add(MjCard.Tiao_6_4);

        s_Cards.Add(MjCard.Tiao_7_1);
        s_Cards.Add(MjCard.Tiao_7_2);
        s_Cards.Add(MjCard.Tiao_7_3);
        s_Cards.Add(MjCard.Tiao_7_4);

        s_Cards.Add(MjCard.Tiao_8_1);
        s_Cards.Add(MjCard.Tiao_8_2);
        s_Cards.Add(MjCard.Tiao_8_3);
        s_Cards.Add(MjCard.Tiao_8_4);

        s_Cards.Add(MjCard.Tiao_9_1);
        s_Cards.Add(MjCard.Tiao_9_2);
        s_Cards.Add(MjCard.Tiao_9_3);
        s_Cards.Add(MjCard.Tiao_9_4);

        s_Cards.Add(MjCard.Wan_1_1);
        s_Cards.Add(MjCard.Wan_1_2);
        s_Cards.Add(MjCard.Wan_1_3);
        s_Cards.Add(MjCard.Wan_1_4);

        s_Cards.Add(MjCard.Wan_2_1);
        s_Cards.Add(MjCard.Wan_2_2);
        s_Cards.Add(MjCard.Wan_2_3);
        s_Cards.Add(MjCard.Wan_2_4);

        s_Cards.Add(MjCard.Wan_3_1);
        s_Cards.Add(MjCard.Wan_3_2);
        s_Cards.Add(MjCard.Wan_3_3);
        s_Cards.Add(MjCard.Wan_3_4);

        s_Cards.Add(MjCard.Wan_4_1);
        s_Cards.Add(MjCard.Wan_4_2);
        s_Cards.Add(MjCard.Wan_4_3);
        s_Cards.Add(MjCard.Wan_4_4);

        s_Cards.Add(MjCard.Wan_5_1);
        s_Cards.Add(MjCard.Wan_5_2);
        s_Cards.Add(MjCard.Wan_5_3);
        s_Cards.Add(MjCard.Wan_5_4);

        s_Cards.Add(MjCard.Wan_6_1);
        s_Cards.Add(MjCard.Wan_6_2);
        s_Cards.Add(MjCard.Wan_6_3);
        s_Cards.Add(MjCard.Wan_6_4);

        s_Cards.Add(MjCard.Wan_7_1);
        s_Cards.Add(MjCard.Wan_7_2);
        s_Cards.Add(MjCard.Wan_7_3);
        s_Cards.Add(MjCard.Wan_7_4);

        s_Cards.Add(MjCard.Wan_8_1);
        s_Cards.Add(MjCard.Wan_8_2);
        s_Cards.Add(MjCard.Wan_8_3);
        s_Cards.Add(MjCard.Wan_8_4);

        s_Cards.Add(MjCard.Wan_9_1);
        s_Cards.Add(MjCard.Wan_9_2);
        s_Cards.Add(MjCard.Wan_9_3);
        s_Cards.Add(MjCard.Wan_9_4);

        s_Cards.Add(MjCard.Dong_1);
        s_Cards.Add(MjCard.Dong_2);
        s_Cards.Add(MjCard.Dong_3);
        s_Cards.Add(MjCard.Dong_4);

        s_Cards.Add(MjCard.Nan_1);
        s_Cards.Add(MjCard.Nan_2);
        s_Cards.Add(MjCard.Nan_3);
        s_Cards.Add(MjCard.Nan_4);

        s_Cards.Add(MjCard.Xi_1);
        s_Cards.Add(MjCard.Xi_2);
        s_Cards.Add(MjCard.Xi_3);
        s_Cards.Add(MjCard.Xi_4);

        s_Cards.Add(MjCard.Bei_1);
        s_Cards.Add(MjCard.Bei_2);
        s_Cards.Add(MjCard.Bei_3);
        s_Cards.Add(MjCard.Bei_4);

        s_Cards.Add(MjCard.Zhong_1);
        s_Cards.Add(MjCard.Zhong_2);
        s_Cards.Add(MjCard.Zhong_3);
        s_Cards.Add(MjCard.Zhong_4);

        s_Cards.Add(MjCard.Fa_1);
        s_Cards.Add(MjCard.Fa_2);
        s_Cards.Add(MjCard.Fa_3);
        s_Cards.Add(MjCard.Fa_4);

        s_Cards.Add(MjCard.Bai_1);
        s_Cards.Add(MjCard.Bai_2);
        s_Cards.Add(MjCard.Bai_3);
        s_Cards.Add(MjCard.Bai_4);
    }

    public static void Shuffle<T>(List<T> list)
    {
        int lastIndex = list.Count - 1;
        for (int i = 0; i <= lastIndex; ++i)
        {
            int rnd = Random.Range(0, lastIndex);
            var temp = list[rnd];
            int swapIndex = lastIndex - i;
            list[rnd] = list[swapIndex];
            list[swapIndex] = temp;
        }
    }

    public static void CopyCards(List<MjCard> list)
    {
        list.Clear();
        list.AddRange(s_Cards);
    }

    public static int GetMaxNumId()
    {
        return 4 * 10 + 3; //白的数字最大
    }

    public static int GetMaxCardNum()
    {
        return s_Cards.Count;
    }

    public static MjCard GetCard(int id)
    {
        return s_Cards[id - 1];
    }

    private int m_Id;
    private int m_Type;
    private int m_Num;

    public int Id { get { return m_Id; } }
    public int Type { get { return m_Type; } }
    public int Num { get { return m_Num; } }

    public int NumId { get { return m_Type * 10 + m_Num; } }

    public string GetCardShow()
    {
        if (m_Type < 3)
            return $"{m_Num}\n{Card_Type_Name[m_Type]}";
        else if (3 == m_Type)
        {
            if (0 == m_Num) return $"东\n{m_Id}";
            else if (3 == m_Num) return $"南\n{m_Id}";
            else if (6 == m_Num) return $"西\n{m_Id}";
            else if (9 == m_Num) return $"北\n{m_Id}";
        }
        else if (4 == m_Type)
        {
            if (0 == m_Num) return $"中\n{m_Id}";
            else if (3 == m_Num) return $"发\n{m_Id}";
            else if (6 == m_Num) return $"白\n{m_Id}";
        }
        return "";
    }

    public string UserFlag { get; set; }

    private string m_Name;
    private string m_Icon;
    private string m_Voice;

    private MjCard(int id, int type, int num)
    {
        m_Id = id;
        m_Type = type;
        m_Num = num;
    }

}
