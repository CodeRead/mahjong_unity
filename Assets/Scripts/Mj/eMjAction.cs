﻿
public enum eMjAction
{
    None = 0,
    Start, //开局发牌
    Fa, //回合发牌
    Chu, //出牌
    Chi, //吃, 吃 -> 不摸, 直接打一张
    Peng, //碰, 碰 -> 不摸, 直接打一张

    Gang, //杠别人, 杠 -> 摸一张 -> 打一张
    AnGang, //暗杠, 摸一张 -> 暗杠 -> 再摸一张 -> 打一张
    PengGang, //碰变杠
    Ting, //听
    ZiMo, //自摸

    Hu, //胡别人
    Guo, //过

    End,
}

//4次碰后, 剩1张牌
//4次杠后, 剩1张牌

