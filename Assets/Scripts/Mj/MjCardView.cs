﻿using UnityEngine;
using UnityEngine.UI;

public class MjCardView : MonoBehaviour
{

    public Image m_ImgBg;
    public Text m_TxtNum;

    public MjCard m_Card; //关联的牌对象

    //显示牌正面
    public void ShowCardNum(MjCard card)
    {
        m_Card = card;
        if (null != card)
            m_TxtNum.text = card.GetCardShow();
    }

    //显示牌背面
    public void ShowCardBack()
    {
        m_Card = null;
        m_TxtNum.text = "";
    }

    public void HideCard()
    {
        m_Card = null;
        this.gameObject.SetActive(false);
    }

}
