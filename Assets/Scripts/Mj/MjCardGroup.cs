﻿
//一个副露
public class MjCardGroup
{
    private eMjAction m_Action = eMjAction.None;
    private MjCard m_Other; //别人的那张牌
    private int m_OtherSitIndex; //别人的那张牌是谁的

    private int m_MyCardNum;
    private MjCard[] m_My = new MjCard[4]; //自己的牌

    public MjCardGroup() { }

    public void Set3CardAction(eMjAction action, MjCard other, eMjSitIndex otherSitIndex, MjCard my1, MjCard my2)
    {
        m_Action = action;
        m_Other = other;
        m_OtherSitIndex = (int)otherSitIndex;

        m_My[0] = my1;
        m_My[1] = my2;
        m_My[2] = null;
        m_My[3] = null;
        m_MyCardNum = 2;
    }

    public void GangAction(MjCard other, eMjSitIndex otherSitIndex, MjCard my1, MjCard my2, MjCard my3)
    {
        m_Action = eMjAction.Gang;
        m_Other = other;
        m_OtherSitIndex = (int)otherSitIndex;

        m_My[0] = my1;
        m_My[1] = my2;
        m_My[2] = my3;
        m_My[3] = null;
        m_MyCardNum = 3;
    }

    public void AnGangAction(MjCard my1, MjCard my2, MjCard my3, MjCard my4)
    {
        m_Action = eMjAction.Gang;
        m_Other = null;
        m_OtherSitIndex = -1;

        m_My[0] = my1;
        m_My[1] = my2;
        m_My[2] = my3;
        m_My[3] = my4;
        m_MyCardNum = 4;
    }

    public MjCard GetMyCard(int index)
    {
        return m_My[index];
    }

    public int MyCardNum
    {
        get { return m_MyCardNum; }
    }

    public int OtherSitIndex
    {
        get { return m_OtherSitIndex; }
    }

    public MjCard GetOtherCard()
    {
        return m_Other;
    }

}
