﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MjSitPlayerView : MonoBehaviour
{

    public RectTransform m_HandCardsTrans;
    //public MjCardView[] m_HandCardViews;

    public RectTransform m_GroupsTrans;
    //public MjCardGroupView[] m_GroupViews;

    public RectTransform m_OutCardsTrans;
    //public MjCardView[] m_OutCardViews;

    public Text m_TxtNick;
    public Text m_TxtScore;
    public Image m_ImgAvatar;

    private int m_ShowOutCardCount;

    //******************** 摸的牌

    public void ShowMoPaiCardNum(MjCard c)
    {
        var trans = m_HandCardsTrans.Find("MoPai/Card");
        var view = trans.GetComponent<MjCardView>();
        view.gameObject.SetActive(true);
        view.ShowCardNum(c);
    }

    public void ShowMoPaiCardBack()
    {
        var trans = m_HandCardsTrans.Find("MoPai/Card");
        var view = trans.GetComponent<MjCardView>();
        view.gameObject.SetActive(true);
        view.ShowCardBack();
    }

    public MjCard HideMoPaiCard()
    {
        var trans = m_HandCardsTrans.Find("MoPai/Card");
        var view = trans.GetComponent<MjCardView>();
        view.gameObject.SetActive(false);
        var c = view.m_Card;
        view.m_Card = null;
        return c;
    }

    //********************

    //******************** 手牌

    //自己的牌显示卡面
    public void ShowHandCardNum(int index, MjCard c)
    {
        var trans = m_HandCardsTrans.Find($"Card ({index})");
        var view = trans.GetComponent<MjCardView>();
        view.gameObject.SetActive(true);
        view.ShowCardNum(c);
    }

    //其他人的牌显示卡背
    public void ShowHandCardBack(int index)
    {
        var trans = m_HandCardsTrans.Find($"Card ({index})");
        var view = trans.GetComponent<MjCardView>();
        view.gameObject.SetActive(true);
        view.ShowCardBack();
    }

    public MjCard HideHandCard(int index)
    {
        var trans = m_HandCardsTrans.Find($"Card ({index})");
        var view = trans.GetComponent<MjCardView>();
        view.gameObject.SetActive(false);
        var c = view.m_Card;
        view.m_Card = null;
        return c;
    }

    //********************

    //******************** 附露

    public void ShowCardGroup(int index, MjCardGroup cardGroup)
    {
        var trans = m_GroupsTrans.Find($"Group ({index})");
        trans.gameObject.SetActive(true);
        
        var view = trans.GetComponent<MjCardGroupView>();
        if (2 == cardGroup.MyCardNum)
        {
            for (int i = 0; i < 2; ++i)
            {
                view.m_Cards[i].gameObject.SetActive(true);
                view.m_Cards[i].ShowCardNum(cardGroup.GetMyCard(i));
            }
            view.m_Cards[2].HideCard();
            view.m_Cards[3].gameObject.SetActive(true);
            view.m_Cards[3].ShowCardNum(cardGroup.GetOtherCard());
        }
        else if (3 == cardGroup.MyCardNum)
        {
            for (int i = 0; i < 3; ++i)
            {
                view.m_Cards[i].gameObject.SetActive(true);
                view.m_Cards[i].ShowCardNum(cardGroup.GetMyCard(i));
            }
            view.m_Cards[3].gameObject.SetActive(true);
            view.m_Cards[3].ShowCardNum(cardGroup.GetOtherCard());
        }
        else if (4 == cardGroup.MyCardNum)
        {
            for (int i = 0; i < 4; ++i)
            {
                view.m_Cards[i].gameObject.SetActive(true);
                view.m_Cards[i].ShowCardNum(cardGroup.GetMyCard(i));
            }
        }
    }

    //附录只会多不会少
    //public void HideCardGroup(int index, MjCardGroup cardGroup)
    //{
    //    var trans = m_GroupsTrans.Find($"Group ({index})");
    //    var view = trans.GetComponent<MjCardGroupView>();
    //    for (int i = 0; i < 4; ++i)
    //    {
    //        view.m_Cards[i].HideCard();
    //    }
    //}

    //********************

        //******************** 已出的牌

    public int ShowOutCardCount
    {
        get { return m_ShowOutCardCount; }
    }

    //增加一张已出牌的显示
    public void PushShowOutCard(MjCard c)
    {
        var trans = m_OutCardsTrans.Find($"Card ({m_ShowOutCardCount++})");
        var view = trans.GetComponent<MjCardView>();
        view.gameObject.SetActive(true);
        view.ShowCardNum(c);
    }

    public MjCard PopShowOutCard()
    {
        var trans = m_OutCardsTrans.Find($"Card ({m_ShowOutCardCount--})");
        var view = trans.GetComponent<MjCardView>();
        view.gameObject.SetActive(false);
        var c = view.m_Card;
        view.m_Card = null;
        return c;
    }

    //********************

    public void SetCardClickable(bool b)
    {
        var cgp = m_HandCardsTrans.GetComponent<CanvasGroup>();
        if (null != cgp)
        {
            cgp.blocksRaycasts = b;
        }
    }

}

