using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public static class SocketHelper
{

    public struct ThreadMessage
    {
        public string m_Cmd;
        public string m_Content;
        public object m_Obj;
        //public object m_Obj2;
    }

    public struct BeginReceiveState
    {
        public Socket m_Socket;
        public byte[] m_Buff;
        public int m_PacketRecvLen;
        public byte[] m_Temp4Bytes;

        public int BuffRemainLen
        {
            get { return m_Buff.Length - m_PacketRecvLen; }
        }
    }


    public struct BeginSendState
    {
        public Socket m_Socket;
        public byte[] m_Bytes; //要发送的数据
        public int m_SendLen; //已发送字节数
    }


    public static string GetIpV4Address()
    {
        // 获取本地主机的相关信息
        var hostName = Dns.GetHostName();
        Debug.Log($"hostName:{hostName}");
        IPHostEntry host = Dns.GetHostEntry(hostName);

        // 遍历所有IP地址
        string result = "";
        foreach (var ipAddress in host.AddressList)
        {
            if (IPAddress.IsLoopback(ipAddress)) continue; //非本地链接地址
            if (ipAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6) continue; //IPv6地址

            Debug.Log($"{ipAddress.ToString()}: ipv6LinkLocal:{ipAddress.IsIPv6LinkLocal}, ipv6Multi:{ipAddress.IsIPv6Multicast}, ipv6SiteLocal:{ipAddress.IsIPv6SiteLocal}, ipv6Teredo:{ipAddress.IsIPv6Teredo}");
            if (ipAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            {
                result = ipAddress.ToString();
            }
        }

        return result;
    }

    public static void ConvertInt32To4Bytes(int v, byte[] bytes, int offset)
    {
        //int转byte[]
        if (BitConverter.IsLittleEndian) //[0]放最低的1-byte
        {
            bytes[offset] = (byte)(v & 0xFF);
            bytes[offset + 1] = (byte)((v >> 8) & 0xFF); //右移时, 最左侧补符号位
            bytes[offset + 2] = (byte)((v >> 16) & 0xFF);
            bytes[offset + 3] = (byte)((v >> 24) & 0xFF);
        }
        else
        {
            bytes[offset] = (byte)((v >> 24) & 0xFF); //从高bits开始写
            bytes[offset + 1] = (byte)((v >> 16) & 0xFF);
            bytes[offset + 2] = (byte)((v >> 8) & 0xFF);
            bytes[offset + 3] = (byte)(v & 0xFF);
        }
    }

    public static void ConvertInt16To2Bytes(short v, byte[] bytes, int offset)
    {
        if (BitConverter.IsLittleEndian) //[0]放最低的1-byte
        {
            bytes[offset] = (byte)(v & 0xFF);
            bytes[offset + 1] = (byte)((v >> 8) & 0xFF); //右移时, 最左侧补符号位
        }
        else
        {
            bytes[offset] = (byte)((v >> 8) & 0xFF); //从高bits开始写
            bytes[offset + 1] = (byte)(v & 0xFF);
        }
    }

    public static void Close(Socket so)
    {
        try
        {
            if (null != so)
                so.Close(); //同Stream那样, Close后不能再使用
        }
        catch (Exception ex)
        {
            Debug.LogWarning($"{ex.GetType().Name}:{ex.Message}");
        }
    }

    public static void ShutdownAndClose(Socket so)
    {
        try
        {
            if (null != so)
            {
                so.Shutdown(SocketShutdown.Both); //Shutdown无法唤醒线程中阻塞的Receive, Close才行
                so.Close();
            }
        }
        catch (Exception ex)
        {
            Debug.LogWarning($"{ex.GetType().Name}:{ex.Message}");
        }
    }

    public static long CurrentTimeMillis()
    {
        var ts = DateTime.Now - new DateTime(1970, 1, 1);
        long ms = (long)ts.TotalMilliseconds;

        long ticks = DateTime.Now.Ticks - 1970L * 365 * 24 * 60 * 60 * 1000 * 10000;
        long ms2 = ticks / 10000;
        Debug.Log($"CurrentTimeMillis: {ms - ms2}");

        return ms;
    }

    public static void RemoveNullElements<T>(List<T> list) where T : class
    {
        int ct = list.Count;
        if (ct <= 0) return;

        int j = 0;
        for (int i = 0; i < ct; ++i)
        {
            if (null == list[i])
            {
                j = i;
                i++;
                //非null元素前移
                for (; i < ct; ++i)
                {
                    if (null != list[i])
                        list[j++] = list[i];
                }
                break;
            }
        }
        
        list.RemoveRange(j, ct - j);
    }

}
