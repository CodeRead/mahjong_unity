﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class LayerDim : MonoBehaviour
{
    private static Stack<Transform> m_Stack = new Stack<Transform>();
    private static Transform m_DimTrans;

    void OnEnable()
    {
        if (null == m_DimTrans)
            m_DimTrans = GameObject.Find("Canvas/Dim").transform;
        m_Stack.Push(this.transform);
        m_DimTrans.SetParent(m_Stack.Peek(), false);
        m_DimTrans.SetAsFirstSibling();
    }

    private void OnDisable()
    {
        m_Stack.Pop();
        if (m_Stack.Count > 0)
        {
            m_DimTrans.SetParent(m_Stack.Peek(), false);
            m_DimTrans.SetAsFirstSibling();
        }
    }


}
